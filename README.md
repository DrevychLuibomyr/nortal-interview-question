# Nortal-Interview-Question



# Requirements

•	Images carousel: when user swipe left or right, list content should change accordingly. The images carousel can handle any number of images.

•	The list: when user scrolls up the whole page should scroll with it. The list can handle any number of items.
P.S. images and list content can be local or loaded from the internet.

•	Search: Search bar should pin top when it reaches screen top. When user enter text, it should filter the labels in the list based on the user input.

•	Don’t use third party libraries.

***


# Explanation of requirements from the executive

1.  Images carousel: when user swipe left or right, list content should change accordingly. The images carousel can handle any number of images.
    
    A carousel containing random data was created to display content. This amount of random data is generated every time in a different way, that is, this list can contain as much content as you want, and the amount of content changes every time, for more information, see ContentViewModel
    Every time the user swipes, the content content changes according to the selected item, which will make a new list of the best movies.

Clarification: when you go to the ViewModel do not be surprised + 1 to the value of the current page. This is doable so you don't get an error from the API

2.  The list: when user scrolls up the whole page should scroll with it. The list can handle any number of items.
P.S. images and list content can be local or loaded from the internet.

    The list contains the content of any number of content models that we receive from the network.

3.  Search: Search bar should pin top when it reaches screen top. When user enter text, it should filter the labels in the list based on the user input.
    
    When the user scrolls the content, the search foams to the top. Also the content is filtered when the user types the title of the movie

# Result 
[Demo of work](https://drive.google.com/file/d/1Lja0c9x9F7987pFyAD6p9fRVtgXjLuw1/view?usp=share_link)

