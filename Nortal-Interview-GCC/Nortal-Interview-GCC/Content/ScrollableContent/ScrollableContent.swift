//
//  ScrollableContent.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import SwiftUI

struct ScrollableContent: View {
    
    @Binding var searchText: String
    @Binding var currentPage: Int
    
    let carouselModel: [CarouselModel]
    var filteredMovies: [MovieResult]
    
    var body: some View {
        VStack(spacing: 0) {
            HeaderView(currentPage: $currentPage, carouselModel: carouselModel)
            LazyVStack(alignment: .leading, pinnedViews: [.sectionHeaders]) {
                MovieListView(searchText: $searchText, filteredMovies: filteredMovies)
                EmptyMovieListPlaceholder(filteredMovies: filteredMovies)
            }
        }
    }
}
