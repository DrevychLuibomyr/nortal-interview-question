//
//  EmptyMovieListPlaceholder.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import SwiftUI

struct EmptyMovieListPlaceholder: View {
    
    var filteredMovies: [MovieResult]
    
    var body: some View {
        if filteredMovies.isEmpty {
            Text("No movies found")
                .foregroundColor(.gray)
                .padding([.horizontal, .top], 10)
                .id("top")
        }
    }
}
