//
//  PosterImage.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import SwiftUI

struct PosterImage: View {
    
    let movie: MovieResult
    
    private var posterURL: URL? {
        URL(string: "https://image.tmdb.org/t/p/w185\(movie.poster ?? "")")
    }
    
    var body: some View {
        AsyncImage(url: posterURL , content: { phase in
            if let image = phase.image {
                image
                    .frame(width: 30, height: 30)
                    .cornerRadius(8.0)
            } else {
                Image("horse")
                    .resizable()
                    .frame(width: 30, height: 30)
                    .cornerRadius(8.0)
            }
        })
    }
}
