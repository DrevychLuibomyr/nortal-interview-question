//
//  MovieList.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import SwiftUI

struct MovieListView: View {
    
    @Binding var searchText: String
    
    var filteredMovies: [MovieResult]
    
    var body: some View {
        Section {
            ForEach(filteredMovies) { movie in
                MovieListItem(movie: movie)
            }
            .padding(.horizontal, 10)
        } header: {
            SearchBar(text: $searchText)
                .padding(.horizontal)
                .background(Color.white)
        }
    }
}
