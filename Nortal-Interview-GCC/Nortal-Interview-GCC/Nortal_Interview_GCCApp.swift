//
//  Nortal_Interview_GCCApp.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import SwiftUI

@main
struct Nortal_Interview_GCCApp: App {
    var body: some Scene {
        WindowGroup {
            let provider = TopRatedMovieProvider()
            ContentView(viewModel: ContentViewModel(provider: provider))
        }
    }
}
