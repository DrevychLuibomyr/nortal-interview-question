//
//  Config.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import Foundation

final class Config {
    
    fileprivate enum Keys {
        static let secret = "SECRET"
    }
    
    static let secret: String = {
        guard let secret = Bundle.main.infoDictionary?[Keys.secret] as? String else {
            fatalError("NO SECRET") //bad practice but for now ok, just to undestand that we have API_Key which we will use 
        }
        return secret
    }()
    
}
