//
//  Endpoint.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import Foundation

protocol Endpoint {
    var method: HTTPMethod { get }
    var path: String { get }
    var headers: [String : String] { get }
    var body: Data? { get }
}
