//
//  HTTPMethod.swift
//  Nortal-Interview-GCC
//
//  Created by ldrevych on 23.08.2023.
//

import Foundation

enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
}
